import {forEach, merge, values} from 'lodash/fp';

import getPlugin from './plugins/get';
import wgetPlugin from './plugins/wget';

const plugins = {
  http_get: getPlugin,
  http_wget: wgetPlugin,
};

// Arguments common to all plugins.
forEach((p) => {
  p.argv = merge({ // eslint-disable-line no-param-reassign
    'http.download_dir': {
      type: 'string',
      nargs: 1,
      default: 'downloads',
      desc: 'The path to the download directory.',
    },
  }, p.argv);
}, values(plugins));


export { plugins };
export default { plugins };
